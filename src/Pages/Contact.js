import React from 'react';
import { Row, Col, Form, Button } from 'react-bootstrap';


export default function Contact () {
	return(
		<div>
			<h1>Contact Me</h1>

			<Row>
				<Col>
					<h2>Details</h2>
					<h4>Phone:</h4>
					<p><a href="tel:+639176558586">+63 917 655 8586</a></p>
					<h4>Email:</h4> 
					<p>arnaizjankristin@gmail.com</p>
					<h4>Address:</h4>  
					<p>Cainta, Rizal, 1900</p>
					<h4>Social Network:</h4>					
					<p><a href="https://www.linkedin.com/in/tinarnaiz/">LinkedIn</a></p>
				</Col>
				<Col>
					<h2>Send Me a Message</h2>
						<Form action="#">

							<Form.Group controlId="courseId">
								<Form.Label>Name</Form.Label>
								<Form.Control type="text" placeholder="Enter Name" required/>
							</Form.Group>

							<Form.Group controlId="courseId">
								<Form.Label>Email</Form.Label>
								<Form.Control type="email" placeholder="Enter Email" required/>
							</Form.Group>

							<Form.Group controlId="courseId">
								<Form.Label>Message</Form.Label>
								<Form.Control s="textarea" rows="3" placeholder="Enter Message" required/>
							</Form.Group>

    						<Button variant="success" type="submit"> Send Message </Button>
						</Form>
				</Col>

			</Row>
						
			
		</div>
		)
}

