import React from 'react';
import Container from 'react-bootstrap/Container';
import myPic from '../mypic.jpg'
 
export default function Home () {
	return(
		<div>
			<Container>				
					<h1>About Me</h1>
					<img src={ myPic } alt="this is me"/>
					<h2>I'm a self-motivated Web Developer with knowledge in programming and designing media as well as skills and ability in writing clean and efficient code.

						My goal is to help individuals and
						businesses to develop and maintain 
						their websites.</h2>
					
			</Container>
			
		</div>
		)
}

	